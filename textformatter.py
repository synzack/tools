#!/usr/bin/python
# -*- coding: utf-8 -*-

# Formatte un texte selon des règles définies parilisateur.
# Les règles sont des lignes commençant par des tabulations
# Étapes : 
# - rechercher et appliquer les règles
# - limiter le nombre de colonnes en respectant l'indentation
# - place le sommaire là où on le demande.

import plac
import re
from string import ascii_lowercase
from textwrap import TextWrapper, fill as w_fill, wrap

# Default config

COLUMNS = 80
#           function    command   aliases
COMMANDS = {
            "plain":   ("plain",  "p"),
            "list":    ("list",   "l"),
            "center":  ("center", "c"),
            "title":   ("title",  "t"),
            "margin":  ("margin", "m"),
           }
TITLE_MAXLEVEL = 6
# see the number function
NUMBERING = "I.1.a)i)" 
SUMMARY = "{{SUMMARY}}"
SUMMARY_INDENT = 6
LIST_INDENT = 2

# end config


class Context(object):
    title_levels = TITLE_MAXLEVEL
    title_numbers = title_levels * [0]
    summary = []


class Block(object):
    wrapper = TextWrapper(width=COLUMNS)
    def __init__(self, content, context):
        if not content:
            self.parsed = ""
        else:
            self.context = context
            command = content.split()[0]
            args = content.split("\n")[0].split()[1:]
            content = content.split("\n", 1)[1:]
            content = content[0] if content else ""
            com = [c for c in COMMANDS.items() if command in c[1]]
            if com and hasattr(self, com[0][0]):
                self.parsed = getattr(self, com[0][0])(content, *args)
            else:
                self.parsed = content

    def _getint(self, args, pos, default):
        """ Returns the argument converted, or the default value """
        try:
            return int(args[pos])
        except (ValueError, IndexError):
            return default

    def _setwrapper(self, content, function, **args):
        """ apply the function with the given options for the wrapper """
        wrap = self.wrapper
        def apply(d):
            for a, v in d.items():
                setattr(wrap, a, v)
        save = {}
        for attr in dir(wrap):
            a = getattr(wrap, attr)
            if not hasattr(a, "__call__") and not attr.startswith("_"):
                save[attr] = a
        apply(args)
        ret = function(content)
        apply(save)
        return ret

    def _wrap(self, content, **args):
        return self._setwrapper(content, self.wrapper.wrap, **args)

    def _fill(self, content, **args):
        return self._setwrapper(content, self.wrapper.fill, **args)

    def center(self, content, *args):
        fill = args[0][0] if args else " "
        return "\n".join(l.center(COLUMNS, fill) for l in
                content.split("\n"))

    def list(self, content, *args):
        if args:
            p = args[0]
            pattern = p[0], p[1:]
        else:
            pattern = None
        lines = []
        text = re.compile("(\n|^)[ \t]*[-+\*] ?").sub("\n\t-", content)
        for i, e in enumerate(text.split("\n\t-")[1:]):
            n = number([i+1], pattern=[pattern]) if pattern else "-"
            margin = " " * LIST_INDENT
            lines.extend(self._wrap(n+" "+e, initial_indent=margin,
                subsequent_indent=margin+(" " * (len(n) + 1))))
        return "\n".join(lines)

    def lmargin(self, content, *args):
        w = self._getint(args, 0, 2)
        margin = w * " "
        return self._fill(content, initial_indent=margin,
                subsequent_indent=margin)

    def margin(self, content, *args):
        lw = self._getint(args, 0, 2)
        rw = self._getint(args, 1, lw)
        lmargin = lw * " "
        return self._fill(content, initial_indent=lmargin,
                subsequent_indent=lmargin, width=(COLUMNS - rw))

    def plain(self, content, *args):
        return "\n".join(self.wrapper.fill(line) for line in
                content.split("\n"))

    def rmargin(self, content, *args):
        w = self._getint(args, 0, 2)
        return self._fill(content, width=(COLUMNS - w))

    def title(self, content, *args):
        max_level = TITLE_MAXLEVEL
        content = self.plain(content)
        if args:
            if args[1:]:
                try:
                    level = int(args[0])
                    if level > max_level:
                        raise ValueError
                except ValueError:
                    level = 1
                title = " ".join(args[1:])
            else:
                level = 1
                title = args[0]
            numbers = self.context.title_numbers
            numbers[level - 1] += 1
            # clear the sublevels
            numbers[level:] = [0] * (max_level - level)
            self.context.summary.append((number(numbers, pattern=NUMBERING_C),
                title))
            n = number(numbers, level, NUMBERING_C)
            return (self._fill("%s%s %s" % (" " * (2 * level), n, title),
                    subsequent_indent=(" "*(2*level + len(n) + 1))) + "\n" +
                    content)
        else:
            return content
    
def int2roman(n):
    """ Stolen here http://www.daniweb.com/code/snippet216865.html """
    mapping = [("cm", 9*"c"), ("xc", 9*"x"), ("ix", 9*"i"), ("d", 5*"c"),
               ("l", 5*"x"), ("v", 5*"i"),("cd", 4*"c"), ("iv", 4*"i"),
               ("xl", 4*"x")]
    bignums = [("m", 1000), ("c", 100), ("x", 10), ("i", 1)]
    val = ""
    for (char, word) in bignums:
        val = val + (n // word) * char
        n = n % word
    for (shortv, longv) in mapping: 
        val = val.replace(longv, shortv)
    return val

def fill(text, width, right=True, char=" "):
    """ returns a string with the good width, filled with char """
    return text.ljust(width, char) if right else text.rjust(width, char)

def make_summary(summary):
    i = SUMMARY_INDENT * " "
    return "\n" + "\n".join(w_fill(l, width=COLUMNS, subsequent_indent=i,
        initial_indent=i) for l in tabbed(summary,
        SUMMARY_INDENT).splitlines()) + "\n"

def main(in_file: ("Input file", "option")=None,
         out_file: ("Output file", "option")=None):
    content = read_file(in_file)
    parsed = parse(content)
    write_file(parsed, out_file)

def number(numbers:"numbers list", level=0, pattern="I.1.a)i)") -> str:
    """
    Example:
        NUMBERING = "I. 1. a) i)"
        number([3, 1, 2, 3, 2, 0]) -> "III. 1. b) iii) 2)"
        number([3, 1, 2, 3, 2, 0], 1) -> "III."
        number([3, 1, 2, 3, 2, 0], 2) -> "1."
        number([3, 1, 2, 3, 2, 0], 3) -> "b)"
    """
    def nformat(n, l):
        if l > len(pattern) or n <= 0:
            return ""
        c, r = pattern[l-1]
        if c == "I":
            ret = int2roman(n).upper()
        elif c == "i":
            ret = int2roman(n)
        elif c == "a":
            if n < 27:
                ret = ascii_lowercase[n-1]
            else:
                ret = ascii_lowercase[n/26-1] + ascii_lowercase[n%26-1]
        elif c == "1":
            ret = str(n)
        return ret + r
    if 0 < level <= len(numbers):
        return nformat(numbers[level - 1], level)
    elif level == 0:
        return "".join(nformat(n, i+1) for (i, n) in enumerate(numbers))
    else:
        return ""

def parse(text) -> str:
    content = []
    context = Context()
    text = "\n" + text
    text = re.compile("\n {3,}", re.M).sub("\n\t", text)
    #pattern = re.compile("(?:^|\n)\\s+", re.M)
    blocks = text.split("\n\t")
    if blocks[0]:
        blocks[0] = ("%s\n" % COMMANDS["plain"][0]) + blocks[0]
    for b in blocks:
        content.append(Block(b, context).parsed)
    content = ("\n".join(content)).replace(SUMMARY,
            make_summary(context.summary))
    return content

def parse_numbers(numbers):
    """ parse a string like "I. a)" and returns [("I", ". "), ("a", ")")] """
    out = []
    b = ""
    for c in numbers + "i":
        if c in ("a", "i", "I", "1"):
            if b:
                out.append( (b[0], "".join(b[1:])) )
            b = [c]
        else:
            b.append(c)
    return out

def read_file(in_file) -> str:
    """ If in_file is none or equal to "-", read from standart input """
    if not in_file or in_file == "-":
        buffer = []
        line = "\n"
        while True:
            try:
                line = input()
            except EOFError:
                break
            buffer.append(line)
        text = "\n".join(buffer)
    else:
        with open(in_file) as f:
            text = f.read()
    return text

def tabbed(lines, lmargin=0, width=COLUMNS):
    """
    each line contains the same number of columns.
    Very bad algorithm, don't use it to display big tables
    """
    if not lines:
        return ""
    max_w = []
    for i in range(len(lines[0])):
        # Get the biggest entry in each column
        max_w.append(sorted(len(e) for e in list(zip(*lines))[i])[-1])

    overflow_indent = sum(max_w[:-1]) + 1
    max_width_last_c = width - (overflow_indent + lmargin)
    new_lines = []
    for l in lines:
        addl = [[]]
        for i, c in enumerate(l[:-1]):
            addl[0].append(fill(c, max_w[i]))
        if len(l[-1]) > max_width_last_c:
            lines = wrap(l[-1], width=max_width_last_c,
                    subsequent_indent = " "*overflow_indent)
            addl[0].append(lines[0])
            addl.extend([l] for l in lines[1:])
        else:
            addl[0].append(l[-1])
        new_lines.extend(addl)
    return "\n".join(" ".join(l) for l in new_lines)


def write_file(text, out_file):
    """ If out_file is none or equal to "-", write to standart output """
    if not out_file or out_file == "-":
        print(text)
    else:
        with open(out_file, "w") as f:
            f.write(text)

NUMBERING_C = parse_numbers(NUMBERING)

if __name__ == "__main__":
    plac.call(main)

