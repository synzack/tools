#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv, stdin
from pprint import pprint
from itertools import zip_longest
from textwrap import TextWrapper

class Table(object):
    def __init__(self, width=80, command="%table"):
        self.width = width
        self.command = command

    def _get_table(self, buffer):
        if not buffer:
            return []
        table = [[c for c in l.split("\t") if c] for l in buffer]
        table = list(zip(*zip_longest(*table, fillvalue="")))
        columns = len(table[0])
        fixed_size = self.width // columns
        sep = "+" + "+".join(("-" * (fixed_size - 2)) for i in range(columns)) + "+"
        out = [sep]
        wrapper = TextWrapper(fixed_size - 4)
        for line in table:
            line = ([[l.ljust(fixed_size - 4) for l in
                wrapper.wrap(c)] for c in line])
            line = [([' ' * (fixed_size - 4)] if not c else c) for c in line]
            line = list(zip_longest(*line, fillvalue=" " * (fixed_size - 4)))
            out.extend(("| " + " | ".join(l) + " |") for l in line)
            out.append(sep)

        return out

    def parse_default(self, text):
        out = []
        buffer = []
        inside_table = False
        for l in text.split("\n"):
            if l.startswith(self.command):
                inside_table = True
            elif l == "" and inside_table:
                inside_table = False
                out.extend(self._get_table(buffer))
                buffer = 0
            elif inside_table:
                buffer.append(l)
            else:
                out.append(l)

        return "\n".join(out)


if __name__ == "__main__":
    table = Table()

    if argv[1:]:
        with open(argv[1]) as f:
            text = f.read()
    else:
        text, line = "", ""
        while True:
            line = stdin.readline()
            if line:
                text += line
            else:
                break

    print(table.parse_default(text))

